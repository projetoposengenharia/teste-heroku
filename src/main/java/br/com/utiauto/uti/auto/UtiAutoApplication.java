package br.com.utiauto.uti.auto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UtiAutoApplication {

	public static void main(String[] args) {
		SpringApplication.run(UtiAutoApplication.class, args);
	}

}
